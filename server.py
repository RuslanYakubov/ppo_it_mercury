from flask import Flask, request, make_response
import service

app = Flask('ppo_it_5')

@app.route('/', methods=['GET'])
def index():
    return 'hello from prep'

@app.route('/items', methods=['GET'])
def get_positions():
    items = service.get_items()
    if items:
        code = 200
    else:
        code = 405
    return make_response({'items': items}, code)

@app.route('/', methods=['POST'])
def upload():
    data = request.get_data()
    response = service.upload(data)
    if response:
        return make_response({'status': 'ok'}, 200)
    else:
        return make_response({'status': 'error'}, 500)
    
@app.route('/checkout', methods=['GET'])
def checkout():
    destination = request.args.getlist('destination')
    #service.checkout(destination)
    return make_response({'status': 'ok'}, 200)

@app.route('/remote', methods=['GET'])
def remote():
    #remote = service.get_remote()
    remote = []
    
    remote = [
        {'name': 'Системный блок'},
        {'name': 'Маркерная доска'}
    ]
    
    if remote:
        code = 200
    else:
        code = 405
    return make_response({'remote': remote}, code)
