#import db
import datetime
import xlrd

def get_items():
    items = db.get_items()
    return items

def write_data(data):
    file_name = f'table_{datetime.datetime.now()}.xls'
    fd = open(file_name, 'wb')
    fd.write(data)
    fd.close()
    return file_name

def upload(data):
    file_name = write_data(data)
    wb = xlrd.open_workbook(file_name)
    sheet = wb.sheet_by_index(0)
    items = []
    for i in range(1, sheet.nrows):
        items.append({'name': sheet.row_values(i)[1],
                     'size': list(map(float, sheet.row_values(i)[2].split('*'))),
                     'weight': float(sheet.row_values(i)[3])})
    print(items)
    return True
    